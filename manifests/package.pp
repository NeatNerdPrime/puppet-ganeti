# @summary Install packages required for a ganeti node
#
# @api private
#
class ganeti::package {

  assert_private()

  ensure_packages( [
    'ganeti', 'bridge-utils', 'iputils-arping', 'socat', 'kpartx',
    'passwdqc', 'qemu-kvm', 'ganeti-instance-debootstrap'
  ], { ensure  => installed } )

  if $ganeti::with_drbd {
    package { 'drbd-utils':
      ensure => installed,
    }
  }
}
